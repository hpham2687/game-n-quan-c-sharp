﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace oAnQuan
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int score_1=0, score_2=0, turn=1;
        int pl = 1;

        public List<TextBlock> Matrix = new List<TextBlock>();

        public MainWindow()
        {
#region initialize
         InitializeComponent();
            refeshScore();
            Matrix.Add(new TextBlock());
            string pick_section = "";
            string pick_direction = "";
            left.MouseDown += left_mousedown;
            right.MouseDown += right_mousedown;
            #endregion
      
   #region creatboard
            for (int i = 1; i <= 5; i++)
            {
                TextBlock btn = new TextBlock()
                {
                    Text = "OO",
                    Name = "btn" + i + "",
                    Height = 90,
                    Width = 88
                };
                btn.TextWrapping = TextWrapping.Wrap;
                btn.Padding = new Thickness(10);

                Matrix.Add(btn);

                btn.Cursor = Cursors.Hand;
                btn.MouseEnter += Btn_MouseEnter;

                btn.MouseDown += btn_mousedown;
                btn.MouseLeave += Btn_MouseLeave;

                b1.Children.Add(Matrix[i]);
            }
            Matrix.Add(btn6);


            for (int i = 7; i <= 11; i++)
            {
                TextBlock btn = new TextBlock()
                {
                    Text = "OO",
                    Name = "btn" + i + "",
                    Height = 90,
                    Width = 85
                };
                btn.MouseEnter += Btn_MouseEnter;
                btn.MouseLeave += Btn_MouseLeave;

                btn.TextWrapping = TextWrapping.Wrap;
                btn.Padding = new Thickness(10);
          

                Matrix.Add(btn);

                btn.Cursor = Cursors.Hand;

                btn.MouseDown += btn_mousedown;

            }

            for (int i = 11; i >= 7; i--)
            {

                b2.Children.Add(Matrix[i]);
            }

            Matrix.Add(btn12);
            #endregion

            #region chon o va huong di


             void takeTurn()
            {


            }
            // su kien click vao 1 o
            void btn_mousedown(object sender, RoutedEventArgs e)
            {



                TextBlock a = sender as TextBlock;
                pick_section = a.Name.ToString();
                string b = pick_section;
                pl = Int32.Parse(b.Substring(3));
                //MessageBox.Show(pl.ToString());
                if (pl >= 1 && pl <= 5) pl = 1;
                if (pl >= 7 && pl <= 11) pl = 2;
                if (turn != pl) { MessageBox.Show("chua den luot cua may"); return; }
                else
                {
                    lr.Visibility = Visibility.Visible;
                   
                }
                //  MessageBox.Show(pl.ToString());


            }
            // su kien click sang trai
            void left_mousedown(object sender, RoutedEventArgs e)
            {
                Image a = sender as Image;
                pick_direction = a.Name.ToString();
                lr.Visibility = Visibility.Collapsed;
                move(pick_section, pick_direction, pl);
                MessageBox.Show("" + pick_section + "- " + pick_direction + "");
                pick_section = "";
                pick_direction = "";
                refeshScore();
            }
            // su kien click sang phai

            void right_mousedown(object sender, RoutedEventArgs e)
            {
                Image a = sender as Image;
                pick_direction = a.Name.ToString();
                lr.Visibility = Visibility.Collapsed;
                move(pick_section, pick_direction, pl);

                MessageBox.Show("" + pick_section + "- " + pick_direction + "");
                pick_section = "";
                pick_direction = "";
                refeshScore();
            }
            #endregion
      

  }
        private void btn_MouseLeave(object sender, MouseEventArgs e)
        {
            TextBlock a = sender as TextBlock;
            a.Background = Brushes.Transparent;
        }

        private void btn_MouseEnter(object sender, MouseEventArgs e)
        {
            TextBlock a = sender as TextBlock;
            a.Background = Brushes.BlueViolet;

        }
        private void Btn_MouseLeave(object sender, MouseEventArgs e)
        {
            TextBlock a = sender as TextBlock;
            a.Background = Brushes.Transparent;
        }

        private void Btn_MouseEnter(object sender, MouseEventArgs e)
        {
            TextBlock a = sender as TextBlock;
            a.Background = Brushes.BlueViolet;

        }

        public void refeshScore()
        {
            if (turn == 1) {
                score1.Background = Brushes.Aqua;
                score2.Background = Brushes.Transparent;

            }
            if (turn == 2) {
                score2.Background = Brushes.Aqua;
                score1.Background = Brushes.Transparent;

            }

        }

        #region refresh_matrix
        public void refresh()
        {
            b1.Children.Clear();
            b2.Children.Clear();
            for (int i = 1; i <= 5; i++)
            {
                b1.Children.Add(Matrix[i]);
            }

            Matrix.Add(Matrix[6]);

            for (int i = 11; i >= 7; i--)
            {
                b2.Children.Add(Matrix[i]);

            }

            Matrix.Add(Matrix[12]);



        }
        public static void DoEvents()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
             new Action(delegate { }));
        }
        #endregion
     public bool isCheckoQuan()
        {


            return false;
        }

        #region nhapnhay

        public void nhapnhay(int i)
        {
            Matrix[i].Background = Brushes.Aqua;
            Thread.Sleep(500);
            DoEvents();

            Matrix[i].Background = Brushes.Transparent;
            Thread.Sleep(500);
            DoEvents();

            Matrix[i].Background = Brushes.Aqua;
            Thread.Sleep(500);
            DoEvents();

            Matrix[i].Background = Brushes.Transparent;
            DoEvents();

            Matrix[i].Text = ""; // lay het vien bi trong o vua chon
            DoEvents();
            Thread.Sleep(1000);

        }




        #endregion

       

        public void tangGiamChiSo(string pick_direction, int player, ref int chiso_hientai)
        {

            if (player == 1)
            {
                if (pick_direction == "left") chiso_hientai--;
                else if (pick_direction == "right") chiso_hientai++;
            }
            else if (player == 2)
            {
                if (pick_direction == "left") chiso_hientai++;
                else if (pick_direction == "right") chiso_hientai--;


            }

        }

        public void move(string pick_section, string pick_direction, int player)
        {
            int next = 1; // next: kiem tra xem o tiep theo co duoc di tiep hay khong
            for (int i = 1; i <= 12; i++) // tim xem label nao trung de xac dinh vi tri
            {
                if (Matrix[i].Name == pick_section)
                {
                    string str_count = Matrix[i].Text.ToString();
                    int count = str_count.Length; // dem so vien bi trong o
                    int chiso_hientai = 0;

#region xac dinh chi so hien tai(vi tri bat dau de reo) tuy theo nguoi choi 1 hoac 2
                 if (player == 1)
                    {
                        if (pick_direction == "left")
                            chiso_hientai = i - 1;
                        else if (pick_direction == "right")
                            chiso_hientai = i + 1;
                    }
                    else if (player == 2)
                    {
                        if (pick_direction == "left")
                            chiso_hientai = i + 1;
                        else if (pick_direction == "right")
                            chiso_hientai = i - 1;

                    }
                 #endregion
            
     #region nhapnhay
                    nhapnhay(i);
                    #endregion
            
     while (next == 1)
                    {
                        refresh();

                        while (count != 0)
                        {


                            if (chiso_hientai > 12) chiso_hientai = 1;
                            if (chiso_hientai < 1) chiso_hientai = 12;
                            refresh();

                            Matrix[chiso_hientai].Text += "O";
                            DoEvents();
                            refresh();
                            Thread.Sleep(1000);
                            tangGiamChiSo(pick_direction, player, ref chiso_hientai);
                            count--;

#region nếu rải hết
                         if (count == 0)
                            {
#region kiểm tra xem có phải ô tiếp là ô ăn quan ko để dừng

        if (player == 1)
                                {
                                    if (pick_direction == "right")
                                        if (chiso_hientai == 6 || chiso_hientai == 12)
                                        {
                                            MessageBox.Show("o tiep theo la o quan");
                                            next = 0;
                                            break;
                                        }

                                    if (pick_direction == "left")
                                        if (chiso_hientai == 6 || chiso_hientai == 0)
                                        {
                                            MessageBox.Show("o tiep theo la o quan");
                                            next = 0;
                                            break;
                                        }
                                }

                                if (player == 2)
                                {
                                    if (pick_direction == "left")
                                        if (chiso_hientai == 6 || chiso_hientai == 12)
                                        {
                                            MessageBox.Show("o tiep theo la o quan");
                                            next = 0;
                                            break;
                                        }

                                    if (pick_direction == "right")
                                        if (chiso_hientai == 6 || chiso_hientai == 0)
                                        {
                                            MessageBox.Show("o tiep theo la o quan");
                                            next = 0;
                                            break;
                                        }
                                }
                                #endregion


                                int countnext = Matrix[chiso_hientai].Text.ToString().Length; // check xem ô tiếp theo rỗng hay không để ăn (dừng)
                                if (countnext == 0) // nếu ô tiếp là ô trống, ăn
                                {
                                    next = 0;
                                    MessageBox.Show("o tiep theo la o trong.AN THOI");
                                    int addscore = 0;
                                    if (pl==1)
                                    { 
                                            if (pick_direction == "left" && Matrix[chiso_hientai - 1].Text.ToString() != "")
                                            {

                                               if (chiso_hientai - 1 == 0 )
                                                addscore = Matrix[12].Text.ToString().Length * 10;
                                               else if (chiso_hientai - 1 == 6)
                                                addscore = Matrix[chiso_hientai - 1].Text.ToString().Length * 10;

                                            else
                                                addscore = Matrix[chiso_hientai - 1].Text.ToString().Length;

                                            if (chiso_hientai - 1 == 0)
                                                Matrix[12].Text = "an an an";
                                            else
                                            Matrix[chiso_hientai - 1].Text = "an an an";


                                                Thread.Sleep(100);
                                                DoEvents();

                                            if (chiso_hientai - 1 == 0)
                                                Matrix[12].Text = "";
                                            else
                                                Matrix[chiso_hientai - 1].Text = "";

                                            }
                                            else if (pick_direction == "right" && Matrix[chiso_hientai + 1].Text.ToString() != "")
                                            {
                                            if (chiso_hientai + 1 == 12 || chiso_hientai + 1 == 6)
                                                addscore = Matrix[chiso_hientai + 1].Text.ToString().Length * 10;
                                            else
                                                addscore = Matrix[chiso_hientai + 1].Text.ToString().Length;


                                                Matrix[chiso_hientai + 1].Text = "an an an";
                                                Thread.Sleep(100);
                                                DoEvents();
                                                Matrix[chiso_hientai + 1].Text = "";

                                            }
                                    }else if (pl ==2)
                                    {


                                        if (pick_direction == "left" && Matrix[chiso_hientai + 1].Text.ToString() != "")
                                        {
                                            if (chiso_hientai + 1 == 12 || chiso_hientai + 1 == 6)
                                                addscore = Matrix[chiso_hientai + 1].Text.ToString().Length * 10;
                                            else
                                                addscore = Matrix[chiso_hientai + 1].Text.ToString().Length;

                                            Matrix[chiso_hientai + 1].Text = "an an an";
                                            Thread.Sleep(100);
                                            DoEvents();
                                            Matrix[chiso_hientai + 1].Text = "";

                                        }
                                        else if (pick_direction == "right" && Matrix[chiso_hientai - 1].Text.ToString() != "")
                                        {
                                            if (chiso_hientai - 1 == 0)
                                                addscore = Matrix[12].Text.ToString().Length * 10;
                                            else if (chiso_hientai - 1 == 6)
                                                addscore = Matrix[chiso_hientai - 1].Text.ToString().Length * 10;

                                            else
                                                addscore = Matrix[chiso_hientai - 1].Text.ToString().Length;

                                            if (chiso_hientai - 1 == 0)
                                                Matrix[12].Text = "an an an";
                                            else
                                                Matrix[chiso_hientai - 1].Text = "an an an";

                                            Thread.Sleep(100);
                                            DoEvents();


                                            if (chiso_hientai - 1 == 0)
                                                Matrix[12].Text = "";
                                            else
                                                Matrix[chiso_hientai - 1].Text = "";

                                        }








                                    }

                                    if (turn == 1) {                                       
                                        score_1 += addscore;
                                        score1.Content = "Score 1:"+score_1;
                                    }else if (turn == 2)
                                    {
                                        score_2 += addscore;
                                        score2.Content = "Score 2:"+score_2;

                                    }
                                    if (pl == 1) turn = 2;
                                    if (pl == 2) turn = 1;


                                    refresh();
                                }
                                else if (countnext != 0) // nếu đéo pahri ô trống,rải tiếp
                                {
                                    if (chiso_hientai != 6 && chiso_hientai != 12 && chiso_hientai != 0)

                                        #region nhapnhay
                                        Matrix[chiso_hientai].Background = Brushes.Aqua;
                                    Thread.Sleep(500);
                                    DoEvents();

                                    Matrix[chiso_hientai].Background = Brushes.Transparent;
                                    Thread.Sleep(500);
                                    DoEvents();
                                    Matrix[chiso_hientai].Background = Brushes.Aqua;
                                    Thread.Sleep(500);
                                    DoEvents();

                                    Matrix[chiso_hientai].Background = Brushes.Transparent;
                                    Thread.Sleep(500);
                                    DoEvents();
                                    #endregion

         Matrix[chiso_hientai].Text = "";
                                    DoEvents();
                                    refresh();
                                    Thread.Sleep(3000);

                                    tangGiamChiSo(pick_direction, player, ref chiso_hientai);


                                    next = 1;
                                    count = countnext;
                                    // refresh();


                                }

                            }
                            #endregion




                        } 
                    }



                }
            }

        }


    }
}